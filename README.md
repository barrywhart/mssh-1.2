mssh-1.2
========

Based on Ubuntu mssh-1.2 package

When a data center is set up to use jump boxes, the normal Ubuntu mssh command is hard to use because it is not possible to ssh directly to the desired machines.

This is a variation of mssh with a new "--jumpbox" option. This option makes it easy to create a bunch of ssh sessions to a single machine, and from there "branch out" and connect to a list of individual machines. Just like traditional mssh but with an additional "hop".

Here's an example of using it:

mssh --jumpbox root@192.168.1.101 root@192.168.1.103 root@192.168.1.104

Note the new "--jumpbox" option. This tells mssh it should initially connect to that machine, not the machines listed on the command line.

Once the mssh window appears, you'll have a bunch of ssh sessions to the jump box. Now type "ssh " and from the menu, choose "Send hostname". This will send the host names you provided on the command line. Press Enter and you'll now be connected to all the machines you listed.
